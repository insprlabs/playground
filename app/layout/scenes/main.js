import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Scene from '../../core/scene.js';

 class MainScene extends Scene {
    constructor(props) {
        super(props);

    }

    componentDidMount() {

    }

    hasNavigationBar() {
        return true;
    }

    navBarStyle() {
        return {backgroundColor: '#2B402E'}
    }

    renderScene() {
        let {dispatch } = this.props;
        // actions = bindActionCreators({displayProfile, displayProfile}, dispatch);

        return this.props.children;
    }
}

export default connect(
  state => ({ activeRoute: state.homeScene.activeRoute })
)(MainScene)
