import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  Image,
} from 'react-native';

import Scene from '../../core/scene.js';
import Screen from '../../core/screen.js';
import { connect } from 'react-redux';
import { displayProfile, displayHome } from '../../store/actions';
import AppStore from '../../store';

import {
    Page,
    PageViewer,
    BottomSheet
} from '../../components/';


import {
    setAuthenticated,
    setLogout
} from '../../store/actions';

import {
    UserProfile,
    Title,
    BackButton,
    Form,
    Group,
    Widget,
    ActionWidget,
    Separator,
    Button,
    EditableWidget
} from '../../components/general';

import {
    updateUserProfile
} from '../../store/actions';


class ProfileScreen extends Screen {
    constructor(props) {
        super(props);

        this.state = {
            language: null,
            visible: false,
            text: 'Test',
            state: this.props.state,
            firstName: this.props.firstName,
            lastName: this.props.lastName,
            city: this.props.city,
            zipcode: this.props.zipcode
        }
    }

    componentDidMount() {
        // setTimeout(() => {
        //     let {dispatch } = this.props;
        //     dispatch(displayProfile());
        //
        //     setTimeout(() => {
        //         this.props.dispatch(displayHome());
        //     }, 2000);
        //
        // }, 2000);
    }

    static renderTitle (){
        return <Title text={'Profile'}/>
    }

    static renderLeftButton() {
        return (
            <BackButton onPress={() => {
                let {dispatch} = AppStore.store;
                dispatch(displayHome());
            }}/>
        )
    }

    static renderRightButton() {
        // return (
        //     <TouchableOpacity onPress={() => {
        //         let {dispatch} = AppStore.store;
        //         dispatch(displayProfile());
        //     }}>
        //         <Text>Next</Text>
        //     </TouchableOpacity>
        // )
    }

    setHidden() {
        this.setState({
            visible: false
        })
    }
    render() {
        let {width, height} = Dimensions.get('window')

        return (
            <View key={'main'} style={{height: height - 64, width: width, position: 'absolute', left: 0, bottom: 0, backgroundColor: 'white'}}>

                <View
                    style={{
                        flex:1,
                    }}>
                    <Form>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingTop: 20,
                            padding: 15,
                        }}>
                            <Image
                                source={{uri:'https://pbs.twimg.com/profile_images/3765824192/dc0b71e8e69ba8ecee4c9bd261049512_400x400.jpeg'}}
                                style={{
                                    width: 90,
                                    height: 90,
                                    borderRadius: 45,
                                    backgroundColor: '#2B402E',
                                }}/>

                            <Text style={{
                                marginTop: 10,
                                fontWeight: '300',
                                fontSize: 25,
                                color: '#2B402E'
                            }}>Breno R.</Text>
                        </View>

                        <Group>
                            <Separator width={width - 50}/>
                            <EditableWidget
                                onChangeText={(firstName) => this.setState({firstName})}
                                actionTitle={'First Name'}
                                value={this.state.firstName}/>
                            <Separator width={width - 50}/>
                            <EditableWidget
                                onChangeText={(lastName) => this.setState({lastName})}
                                actionTitle={'Last Name'}
                                value={this.state.lastName}/>
                            <Separator width={width - 50}/>
                        </Group>

                        <Group>
                            <Separator width={width - 50}/>
                            <EditableWidget
                                onChangeText={(city) => this.setState({city})}
                                actionTitle={'City'}
                                value={this.state.city}/>
                            <Separator width={width - 50}/>
                            <EditableWidget
                                onChangeText={(state) => this.setState({state})}
                                actionTitle={'State'}
                                value={this.state.state}/>
                            <Separator width={width - 50}/>
                            <EditableWidget
                                onChangeText={(zipcode) => this.setState({zipcode})}
                                keyboardType={'numeric'}
                                actionTitle={'Zip Code'}
                                value={this.state.zipcode}
                            />
                            <Separator width={width - 50}/>
                        </Group>

                        <Group>
                            <View style={{height: 60, padding: 10}}>
                                <Button
                                    onPress={() => {
                                        let {dispatch} = AppStore.store;
                                        let data = {
                                            firstName: this.state.firstName,
                                            lastName: this.state.lastName,
                                            city: this.state.city,
                                            state: this.state.state,
                                            zipcode: this.state.zipcode,
                                        }

                                        // TODO: Call the GetCashAPI and update the user profile.

                                        dispatch(updateUserProfile(data));
                                    }}
                                    text={'Update'}
                                    color={'#2B402E'}
                                    textColor={'white'}/>
                            </View>

                            <Separator width={width - 50}/>
                            <ActionWidget actionTitle={'Change Password'}/>
                            <Separator width={width - 50}/>
                            <ActionWidget actionTitle={'Terms and Agreement'}/>
                            <Separator width={width - 50}/>
                        </Group>

                    </Form>
                    <View style={{height: 60, padding: 10}}>
                        <Button
                            onPress={() => {
                                let {dispatch} = AppStore.store;
                                dispatch(setLogout());
                            }}
                            text={'Log Out'}
                            color={'rgba(152, 149, 132, 0.06)'}
                            textColor={'#27976D'}/>
                    </View>
                </View>
            </View>
        )
    }
}

let profileScreen = connect(
    state => state.userProfile
)(ProfileScreen)

profileScreen.props = ProfileScreen.props;

export default profileScreen;
