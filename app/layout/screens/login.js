
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Navigator,
  Platform,
  TouchableOpacity,
  StatusBar,
  Modal,
  Picker,
  Dimensions,
  Animated,
  LayoutAnimation,
  ScrollView,
  TextInput,
  Image
} from 'react-native';

const DEV = false;

import {
    UserProfile,
    Title,
    SearchButton,
    DropDown,
    ActionButton,
    FilterBar,
    BackButton,
    Form,
    Group,
    Widget,
    ActionWidget,
    Separator,
    Button,
    EditableWidget,
    TitleInput,
    DescriptionInput,
    DropDownWidget,
    BoolWidget,
    CheckWidget,
    InputWidget,
    DropDownTitle
} from '../../components/general';

import {
    Page,
    PageViewer,
    BottomSheet
} from '../../components/';

import { Router, Screen } from '../../core';

import {
    setAuthenticated,
    setLogout,
    displayRegistration
} from '../../store/actions';

/// Model
import AppStore from '../../store';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    logoContainer: {
        flex: 0.7,
        width: width,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginContainer: {
        flex: 2,
        // justifyContent: 'center',
        // alignItems: 'flex-start',
        // margin: 30
    },
    inputContent: {
        color: 'black',
        paddingTop: 2,
        paddingBottom: 5,
        width: width/1.2
    },
    link: {
        color: 'red',
        fontSize: 14,
        textDecorationLine: 'underline',
        marginLeft: 2,
        marginRight: 2
    },
    linkContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        margin: 30
    },
    buttonContainer: {
        marginLeft: 30,
        marginRight: 30,
        marginTop: 5,
        marginBottom: 5,
        justifyContent: 'flex-end',
        height: 38
    }
});

export default class LoginScreen extends Screen {
    constructor(props) {
        super(props);

        this.state = {
            language: null,
            registration: false,
            emailText: null,
            passwordText: null
        }
    }

    static renderTitle() {
        return <View></View>
    }
    static renderLeftButton() {
        return <View></View>
    }
    static renderRightButton() {
        return <View></View>
    }

    renderSignUpButton() {
        return (
            <TouchableOpacity onPress={() => {

                this.setState({
                    registration: true
                });

                // let {dispatch} = AppStore.store;
                // dispatch(displayRegistration());
            }}>
                <Text style={styles.link} >Sign up</Text>
            </TouchableOpacity>
        )
    }

    dismissNewPost() {
        this.setState({
            registration: false
        })
    }

    registerNewUser(height, width) {
        if (this.state.registration)
        return (
            <BottomSheet
                transparent
                native
                height={height - 80}
                color={'rgba(43, 64, 46, 0.8)'}
                visible={this.props.isNewPostVisible}
                setHidden={this.dismissNewPost.bind(this)}
                mainActionText={'Done'}
                titleText={'Register'}
                renderSecundaryScreen={true}
                secundaryContent={
                    <View>
                </View>
                }
            >
                <ScrollView>
                    <Group>
                        <Separator width={width - 50}/>
                        <InputWidget
                            actionTitle={'Email'}
                            keyboardType={'email-address'}
                            keyboardAppearance={'dark'}
                            placeholder={'email@domain.com'}
                        />
                        <Separator width={width - 50}/>
                        <InputWidget
                            actionTitle={'Password'}
                            keyboardAppearance={'dark'}
                            placeholder={'1234567'}
                        />
                        <Separator width={width - 50}/>
                    </Group>
                </ScrollView>
            </BottomSheet>
        )
    }

    // handles the login after the login button is pressed
    _handleLogin() {

    let {dispatch} = AppStore.store;

      // the email
      let email = this.state.emailText;

      // the user password
      let password = this.state.passwordText;

      // verifies if the user is logged
      api.isLogged().then((data)=>{
        // console.log(data);
        // dispatch(setAuthenticated());
      })

      api.signOut()

      // do the basic login

      let response = api.basicLogin("pvsousalima@gmail.com", "123456");

        response.then((data)=>{
            dispatch(setAuthenticated());
            console.log(data)
      });
    }

    render() {
        let {width, height} = Dimensions.get('window')

        return(
            <View style={styles.container} >
                <View style={styles.logoContainer}>

                </View>
                <ScrollView
                    bounces={false}
                    style={styles.loginContainer} >

                    <View style={{alignItems: 'center', marginBottom: 20}}>
                        <Image
                            source={require('./images/logo.png')}
                            style={{width: 150, height: 150}}>

                        </Image>
                    </View>

                    <Group>
                        <Separator width={width - 50}/>
                        <InputWidget
                            autoCapitalize={'none'}
                            actionTitle={'Email'}
                            keyboardType={'email-address'}
                            keyboardAppearance={'dark'}
                            placeholder={'email@domain.com'}
                            onChangeText={(emailText) => this.setState({emailText})}
                            value={this.state.emailText}
                        />
                        <Separator width={width - 50}/>
                        <InputWidget
                            actionTitle={'Password'}
                            keyboardAppearance={'dark'}
                            placeholder={'1234567'}
                            secureTextEntry={true}
                            onChangeText={(passwordText) => this.setState({passwordText})}
                            value={this.state.passwordText}
                        />
                        <Separator width={width - 50}/>
                    </Group>

                    {/*<Label name='Email' />
                        <TextInput
                        placeholder='nome@email.com'
                        keyboardType='email-address'
                        placeholderTextColor='gray'
                        underlineColorAndroid='gray'
                        style={styles.inputContent}
                        />
                        <Label name='Password' />
                        <TextInput
                        placeholder='••••••'
                        placeholderTextColor='gray'
                        underlineColorAndroid='gray'
                        secureTextEntry={true}
                        style={styles.inputContent}
                    />*/}
                </ScrollView>
                <View style={styles.buttonContainer} >
                    <Button
                        color='#2b402e'
                        textColor='white'
                        text='Sign in to GetCashWork'
                        onPress={this._handleLogin.bind(this)}
                    />
                </View>
                <View style={styles.buttonContainer} >
                    <Button
                        text='Connect with Facebook'
                        source={require('../../components/general/images/facebook.png')}
                        color='#3B5998'
                        textColor='white'
                    />
                </View>
                <View style={styles.linkContainer} >
                    <Text style={styles.text} >Don't have an account?</Text>
                    {this.renderSignUpButton()}
                </View>

                {this.registerNewUser(height, width)}
            </View>
        );
    }
}
