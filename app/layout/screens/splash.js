
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Navigator,
  Platform,
  TouchableOpacity,
  StatusBar,
  Modal,
  Picker,
  Dimensions,
  Animated,
  LayoutAnimation,
  ScrollView,
  TextInput,
  Image
} from 'react-native';

const DEV = false;

import {
    UserProfile,
    Title,
    SearchButton,
    DropDown,
    ActionButton,
    FilterBar,
    BackButton,
    Form,
    Group,
    Widget,
    ActionWidget,
    Separator,
    Button,
    EditableWidget,
    TitleInput,
    DescriptionInput,
    DropDownWidget,
    BoolWidget,
    CheckWidget,
    InputWidget,
    DropDownTitle
} from '../../components/general';

import {
    Page,
    PageViewer,
    BottomSheet
} from '../../components/';

import { Router, Screen } from '../../core';

import {
    displayRegistration
} from '../../store/actions';
/// Model
import Store from '../../store';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    logoContainer: {
        flex: 0.7,
        width: width,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginContainer: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'flex-start',
        // margin: 30
    }
});

export default class SplashScreen extends Screen {
    constructor(props) {
        super(props);

        this.state = {
            language: null,
        }
    }

    static renderTitle() {
        return <View></View>
    }
    static renderLeftButton() {
        return <View></View>
    }
    static renderRightButton() {
        return <View></View>
    }


    render() {
        let {width, height} = Dimensions.get('window')
        return(
            <View style={styles.container} >
                <View style={styles.logoContainer}>
                </View>
                <ScrollView
                    bounces={false}
                    style={styles.loginContainer} >

                    <View style={{alignItems: 'center', marginBottom: 20}}>
                        <Image
                            source={require('./images/logo.png')}
                            style={{width: 150, height: 150}}>
                        </Image>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
