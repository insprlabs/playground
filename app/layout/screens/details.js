import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  Image,
  InteractionManager
} from 'react-native';

import Scene from '../../core/scene.js';
import Screen from '../../core/screen.js';
import { connect } from 'react-redux';
import { displayProfile, displayHome } from '../../store/actions';
import AppStore from '../../store';

import {
    Page,
    PageViewer,
    BottomSheet
} from '../../components/';

import {
    UserProfile,
    Title,
    SearchButton,
    DropDown,
    ActionButton,
    FilterBar,
    BackButton,
    Form,
    Group,
    Widget,
    ActionWidget,
    Separator,
    Button,
    EditableWidget,
    TitleInput,
    DescriptionInput,
    JobDescription,
    JobTitle,
    DropDownWidget,
    BoolWidget
} from '../../components/general';

class JobDetailsScreen extends Screen {
    constructor(props) {
        super(props);

        this.state = {
            language: null,
            visible: false,
            text: 'Test',
            load: true
        }
    }

    componentDidMount() {

        InteractionManager.runAfterInteractions(() => {
            this.setState({load: true})
        });

        setTimeout(() => {
            this.setState({load: true})
        }, 250);
    }

    static renderTitle (){
        return <Title text={'Job Details'}/>
    }

    static renderLeftButton() {
        return (
            <BackButton onPress={() => {
                let {dispatch} = AppStore.store;
                dispatch(displayHome());
            }}/>
        )
    }

    static renderRightButton() {
        // return (
        //     <TouchableOpacity onPress={() => {
        //         let {dispatch} = AppStore.store;
        //         dispatch(displayProfile());
        //     }}>
        //         <Text>Next</Text>
        //     </TouchableOpacity>
        // )
    }

    setHidden() {
        this.setState({
            visible: false
        })
    }

    render() {
        let {width, height} = Dimensions.get('window')

        // console.log(this.props.jobDetails);

        if (!this.props.jobDetails) {
            return <View/>
        }

        let {title, description, image} = this.props.jobDetails;

        return (
            <View key={'main'} style={{height: height - 64, width: width, position: 'absolute', left: 0, bottom: 0, backgroundColor: 'white'}}>
                <Form>
                    <View style={{
                        height: 10
                    }}/>
                    <View>
                        <View style={{height: 40, flexDirection: 'row'}}>
                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                margin: 20,
                                marginLeft: 20,
                                marginRight: 10
                            }}>

                                <Image
                                    style={{width: 36, height: 36, borderRadius: 4}}
                                    source={{uri:image}}/>

                        </View>

                        <View style={{
                            flex:1,
                            height: 40,
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                            marginLeft: 0,
                        }}>
                            <Text
                            numberOfLines={1}
                            style={{
                                marginLeft: 0,
                                marginRight: 5,
                                fontSize: 12,
                                backgroundColor: 'transparent',
                                color: '#ABABAB'
                            }}>
                                <Text style={{
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                    color: '#27976D'}}>
                                    Kevin Rose
                                </Text>
                                {` • December 24 • Boston MA`}
                            </Text>
                        </View>
                        </View>
                        <View style={{
                            height: 10
                        }}/>
                        <Separator width={width - 50}/>
                    </View>

                    <Group>
                        <JobTitle
                            value={title}
                         maxLength={100} width={width}/>
                        <Separator width={width - 50}/>
                        <JobDescription
                            value={description}
                        width={width}/>
                    </Group>

                    <Group>
                        <Separator width={width - 50}/>
                        <DropDownWidget
                            inactive
                            actionTitle={'Category'}
                            value={'Home Services'}/>
                        <Separator width={width - 50}/>
                        <DropDownWidget
                            inactive
                            actionTitle={'Type of Job'}
                            value={'Plumber'}/>
                        <Separator width={width - 50}/>
                    </Group>

                    <Group>
                        <Separator width={width - 50}/>
                        <BoolWidget
                            incative
                            actionTitle={'Working From a Place'}
                            value={false}/>
                        <Separator width={width - 50}/>
                        <BoolWidget
                            incative
                            actionTitle={'Type of Job'}
                            value={true}/>
                        <Separator width={width - 50}/>
                    </Group>

                    </Form>
            </View>
        )
    }
}

let jobDetailsScreen = connect(
  state => ({ jobDetails: state.homeScene.jobDetails})
)(JobDetailsScreen)

jobDetailsScreen.props = JobDetailsScreen.props;

export default jobDetailsScreen;
