import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  ListView,
  Image,
  TextInput
} from 'react-native';

import Scene from '../../core/scene.js';
import Screen from '../../core/screen.js';
import { connect } from 'react-redux';
import { displayProfile, displayHome } from '../../store/actions';
import AppStore from '../../store';

import {
    Page,
    PageViewer,
    BottomSheet
} from '../../components/';

import { UserProfile, Title, BackButton, EditableWidget} from '../../components/general';

const { width, height } = Dimensions.get('window');

class SearchScreen extends Screen {
    constructor(props) {
        super(props);

        //let teste = [''];
        let teste = ['aaa', 'vvv', 'ccc', 'ddd'];
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(teste),
            language: null,
            visible: false,
            text: 'Test'
        }
    }

    componentDidMount() {
        // setTimeout(() => {
        //     let {dispatch } = this.props;
        //     dispatch(displayProfile());
        //
        //     setTimeout(() => {
        //         this.props.dispatch(displayHome());
        //     }, 2000);
        //
        // }, 2000);
    }

    static renderTitle (){
        return <Title text={'Search'}/>
    }

    static renderLeftButton() {
        return (
            <BackButton onPress={() => {
                let {dispatch} = AppStore.store;
                dispatch(displayHome());
            }}/>
        )
    }

    static renderRightButton() {
        // return (
        //     <TouchableOpacity onPress={() => {
        //         let {dispatch} = AppStore.store;
        //         dispatch(displayProfile());
        //     }}>
        //         <Text>Next</Text>
        //     </TouchableOpacity>
        // )
    }

    setHidden() {
        this.setState({
            visible: false
        })
    }

    renderBottomSheet() {
        if (this.state.visible)
        return (
            <BottomSheet
                transparent
                native
                height={400}
                color={'rgba(43, 64, 46, 0.8)'}
                visible={this.state.visible}
                setHidden={this.setHidden.bind(this)}
            >
            </BottomSheet>
        )
    }

    renderSearchField() {
        return(
            <View
                style={{
                    height: 40,
                    width: width,
                    backgroundColor: '#435A46',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: 5

                }}
            >
                <TextInput
                    style={{
                        flex: 1,
                        backgroundColor: '#2B402E',
                        borderRadius: 2,
                        paddingLeft: 5,
                        fontSize: 13,
                        color: 'white'
                    }}>
                </TextInput>
            </View>
        );
    }

    renderSearchedKeywords() {
        return(
            <View style= {{
                backgroundColor: 'white',
                width: width,
                height: 30,
                padding: 5
            }}>
                <Text style={{
                    color: '#CACFCB',
                    fontSize: 14
                }}>
                    Recently searched keywords
                </Text>
            </View>
        );
    }

    renderBackgroundInfo() {
        return(
            <View style={{
                // backgroundColor: '#F7F7F7',
                height: height - 100,
                width: width,
                justifyContent: 'center',
                alignItems: 'center'
            }} >
                <Image source={require('../../components/general/images/gray-search-icon.png')}
                    style={{height: 130, width: 130}}/>
                <Text style={{
                    color: '#CACFCB',
                    fontSize: 16,
                    textAlign: 'center',
                    margin: 20,
                    marginLeft: 50,
                    marginRight: 50
                }}>
                    Your recently searched keywords will appear here
                </Text>
            </View>
        );
    }

    renderKeywordList() {
        return(
            <View style={{
                backgroundColor: 'white',
                width: width,
                justifyContent: 'flex-start',
                alignItems: 'flex-start'
            }}>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) =>
                        <Text style={{color: 'gray'}} >
                        {rowData}
                    </Text>}
                />
            </View>
        );
    }

    render() {
        return (
            <View key={'main'} style={{height: (Platform.OS == 'ios') ? height - 64 : height - 54, width: width, position: 'absolute', left: 0, bottom: 0, backgroundColor: '#F7F7F7'}}>
                {this.renderSearchField()}
                {/*{this.renderBottomSheet()}*/}
                <ScrollView
                    bounces={false}
                >
                    {this.renderSearchedKeywords()}
                    {(this.teste === '') ? this.renderBackgroundInfo() : this.renderKeywordList()}
                </ScrollView>
            </View>
        )
    }
}

let searchScreen = connect(
  state => ({ home: state.jobPosts})
)(SearchScreen)

searchScreen.props = SearchScreen.props;

export default searchScreen;
