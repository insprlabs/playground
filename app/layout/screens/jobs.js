import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  ListView,
  Image,
  RefreshControl,
  TextInput
} from 'react-native';

import Scene from '../../core/scene.js';
import Screen from '../../core/screen.js';

import { connect } from 'react-redux';
import AppStore from '../../store';

import {
    displayProfile,
    displayHome,
    displayJobDetails,
    displaySearch,
    displayNewPost,
    hideNewPost,
    displayFilter,
    hideFilter,
    displayCategories,
    hideCategories,
    setActiveCategory,
} from '../../store/actions';


import {
    UserProfile,
    Title,
    SearchButton,
    DropDown,
    ActionButton,
    FilterBar,
    BackButton,
    Form,
    Group,
    Widget,
    ActionWidget,
    Separator,
    Button,
    EditableWidget,
    TitleInput,
    DescriptionInput,
    DropDownWidget,
    BoolWidget,
    CheckWidget,
    DropDownTitle,
    ImageUploadWidget
} from '../../components/general';

import {
    Page,
    PageViewer,
    BottomSheet
} from '../../components/';

import {GetCashAPI} from '../../services'
import usersData from './big.json';
// let {width, height} = Dimensions.get('window')

import shortid from 'shortid';

class Card extends Component {
    render() {
        return (
            <View style={{
                padding: 5,
                paddingBottom: 0,
            }}>
                <TouchableOpacity
                    onPress={this.props.onPress}
                    style={{
                        padding: 5,
                        flex: 1,
                        backgroundColor: 'white',
                        borderRadius: 4,
                        shadowColor: 'black',
                        shadowOffset: {width: 0, height: 1},
                        shadowOpacity: 0.05,
                        shadowRadius: 0.5,
                        borderWidth: 1,
                        borderColor: 'rgba(0, 0, 0, 0.06)'
                    }}>

                    <View style={{height: 40, flexDirection: 'row'}}>
                        <View style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            margin: 5,
                        }}>

                            <Image
                                style={{width: 28, height: 28, borderRadius: 4}}
                                source={{uri: this.props.image }}/>

                        </View>

                        <View style={{
                            flex:1,
                            height: 40,
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                            marginLeft: 2
                        }}>
                            <Text
                                numberOfLines={1}
                                style={{
                                    marginLeft: 0,
                                    marginRight: 5,
                                    fontSize: 12,
                                    backgroundColor: 'transparent',
                                    color: '#ABABAB'
                                }}>
                                <Text style={{
                                    fontSize: 12,
                                    fontWeight: 'bold',
                                color: '#27976D'}}>
                                    {this.props.name}
                                </Text>
                                {` • ${this.props.date} • ${this.props.location}`}
                            </Text>
                        </View>

                        <View style={{
                            height: 40,
                        }}>
                            <View style={{
                                height: 25, backgroundColor: 'black',
                                margin: 5,
                                marginLeft: 0,
                                padding: 5,
                                marginTop: 5,
                                marginBottom: 0,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 15
                            }}>
                                <Text style={{
                                    marginLeft: 10,
                                    marginRight: 10,
                                    backgroundColor: 'transparent',
                                    fontSize: 17,
                                    fontWeight: 'bold',
                                    color: 'white'
                                }}>
                                    {`$${this.props.value}`}
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View style={{marginBottom: 5}}>
                        <Text
                            numberOfLines={2}
                            style={{
                                backgroundColor: 'transparent',
                                padding: 5,
                                paddingTop: 0,
                                paddingBottom: 6,
                                fontSize: 20,
                                fontWeight: 'bold',
                            }}>{this.props.title}</Text>

                        <Text
                            numberOfLines={4}
                            style={{
                                backgroundColor: 'transparent',
                                padding: 5,
                                paddingTop: 0,
                                paddingBottom: 0,
                                fontSize: 13,
                                color: '#6C836E'
                            }}>{this.props.description}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

class JobsScreen extends Screen {
    constructor(props) {
        super(props);

        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
            language: null,
            visible: false,
            text: 'Test',
            dataSource: ds.cloneWithRows(usersData),
            refreshing: false,
            displayScondaryContent: false
        }
    }

    componentDidMount() {
        // setTimeout(() => {
        //     let {dispatch } = this.props;
        //     dispatch(displayProfile());
        //
        //     setTimeout(() => {
        //         this.props.dispatch(displayHome());
        //     }, 2000);
        //
        // }, 2000);
    }

    componentDidUpdate(next, prev) {
        // this.dataSource = this.ds.cloneWithRows(this.props.jobPosts['latest'])
    }

    static renderTitle (dispatch){
        return <DropDownTitle
            onPress={() => {
                let {dispatch} = AppStore.store;
                dispatch(displayCategories());
            }}/>
    }

    static renderLeftButton() {

        return <UserProfile onPress={() => {
            // console.log('dsfsdfdsfdssf', dispatch);

            // console.log(this.props.dispatch);
            let {dispatch} = AppStore.store;
            dispatch(displayProfile());

            // dispatch(displayProfile());
        }}/>
    }

    static renderRightButton() {
        return null;
        // return <SearchButton onPress={() => {
        //     let {dispatch} = AppStore.store;
        //     dispatch(displaySearch());
        // }}/>
    }

    dismissNewPost() {
        let {dispatch} = AppStore.store;
        dispatch(hideNewPost());
    }

    dismissFilter() {
        let {dispatch} = AppStore.store;
        dispatch(hideFilter());
    }

    dismissCategories() {
        let {dispatch} = AppStore.store;
        dispatch(hideCategories());
    }

    makeWidget(key) {
        return <CheckWidget
            onPress={() => {
                let {dispatch} = AppStore.store;
                dispatch(setActiveCategory(key));
                setTimeout(() => {
                    this.bottomSheet.dismissModal();
                }, 400);
            }}
            active={ this.props.activeCategory == key } actionTitle={key}/>
    }

    makeCategoryWidget(key) {
        return <CheckWidget
            onPress={() => {
                let {dispatch} = AppStore.store;
                dispatch(setActiveCategory(key));


                setTimeout(() => {
                    this.setState({displayScondaryContent: false})

                    // this.bottomSheet.dismissModal();
                }, 400);
            }}
            active={ this.props.activeCategory == key } actionTitle={key}/>
    }


    renderSelectCategory(width) {
        let makeListOfCategories = () => {
            let list = this.props.categoriesList;
            // console.log(this.props);
            return list.map(name => {
                return <View key={shortid.generate()}>
                    {this.makeCategoryWidget(name)}
                    <Separator width={width - 50}/>
                </View>
            })
        }

        return (
            // <Group>
            <View>
                {makeListOfCategories()}
            </View>

                // </Group>
        )
    }

    renderNewPost(height, width) {
        if (this.props.isNewPostVisible)
        return (
            <BottomSheet
                transparent
                native
                height={height - 80}
                color={'rgba(43, 64, 46, 0.8)'}
                visible={this.props.isNewPostVisible}
                setHidden={this.dismissNewPost.bind(this)}
                mainActionText={'Publish'}
                titleText={'New Post'}
                displayScondaryContent={this.state.displayScondaryContent}
                renderSecundaryScreen={true}
                onDismissSecondaryContent={() => {
                    this.setState({
                        displayScondaryContent: false
                    });
                }}
                secundaryContent={
                    <View>

                    {this.renderSelectCategory(width)}

                    {/*<TouchableOpacity onPress={() => {
                        this.setState({displayScondaryContent: false})
                        }}>
                        <Text>PrevPage</Text>
                    </TouchableOpacity>*/}
                </View>
                }
            >
                <ScrollView>
                    <Group>
                        <TitleInput maxLength={100} width={width}/>
                        <Separator width={width - 50}/>
                        <DescriptionInput maxLength={250} width={width}/>
                    </Group>

                    <Group>
                        <Separator width={width - 50}/>
                        <EditableWidget
                            actionTitle={'Hourly Rate'}
                            keyboardType={'numeric'}
                            keyboardAppearance={'dark'}
                            value={'25'}/>
                        <Separator width={width - 50}/>
                    </Group>

                    <Group>
                        <Separator width={width - 50}/>
                        <DropDownWidget
                            onPress={() => {
                                this.setState({
                                    displayScondaryContent: true
                                })
                            }}
                            actionTitle={'Category'}
                            value={'Home Services'}/>
                        <Separator width={width - 50}/>
                        <DropDownWidget
                            onPress={() => {
                                this.setState({
                                    displayScondaryContent: true
                                })
                            }}
                            actionTitle={'Type of Job'}
                            value={'Plumber'}/>
                        <Separator width={width - 50}/>
                    </Group>

                    <Group>
                        <ScrollView
                            horizontal={true}
                        >
                            <ImageUploadWidget
                                onSelectImage={(image) => {

                                }}
                            />

                            <ImageUploadWidget
                                onSelectImage={(image) => {

                                }}
                            />

                            <ImageUploadWidget
                                onSelectImage={(image) => {

                                }}
                            />

                            <ImageUploadWidget
                                onSelectImage={(image) => {

                                }}
                            />

                        </ScrollView>
                    </Group>

                    <Group>
                        <Separator width={width - 50}/>
                        <DropDownWidget
                            actionTitle={'Days available to work'}
                            value={'Week Days'}/>
                        <Separator width={width - 50}/>
                        <DropDownWidget
                            actionTitle={'Hours available to work'}
                            value={'Plumber'}/>
                        <Separator width={width - 50}/>
                    </Group>

                    <Group>
                        <Separator width={width - 50}/>
                        <BoolWidget
                            actionTitle={'Working remotely'}
                            value={false}/>
                        <Separator width={width - 50}/>
                        <BoolWidget
                            actionTitle={'Working from a place'}
                            value={false}/>
                        <Separator width={width - 50}/>
                        <BoolWidget
                            actionTitle={'Working from a home'}
                            value={false}/>
                        <Separator width={width - 50}/>
                    </Group>

                    {/*<Group>
                        <Separator width={width - 50}/>
                        <BoolWidget
                        actionTitle={'Share on Facebook'}
                        value={false}/>
                        <Separator width={width - 50}/>
                        <BoolWidget
                        actionTitle={'Share on LinkedIn'}
                        value={false}/>
                        <Separator width={width - 50}/>
                        <BoolWidget
                        actionTitle={'Share on Twitter'}
                        value={false}/>
                        <Separator width={width - 50}/>
                    </Group>*/}
                </ScrollView>
            </BottomSheet>
        )
    }

    renderFilter(height, width) {
        if (this.props.isFilterVisible)
        return (
            <BottomSheet
                transparent
                native
                height={400}
                color={'rgba(43, 64, 46, 0.8)'}
                visible={this.props.isFilterVisible}
                setHidden={this.dismissFilter.bind(this)}
                mainActionText={'Done'}
                titleText={'Filter Options'}
                displayScondaryContent={this.state.displayScondaryContent}
                renderSecundaryScreen={true}
                onDismissSecondaryContent={() => {
                    this.setState({
                        displayScondaryContent: false
                    });
                }}
                secundaryContent={
                    <View>
                    <TouchableOpacity onPress={() => {
                        this.setState({displayScondaryContent: false})
                    }}>
                        <Text>PrevPage</Text>
                    </TouchableOpacity>
                </View>
                }
            >

                <Group>
                    {/*<Separator width={width - 50}/>*/}
                    {/*<DropDownWidget
                        actionTitle={'Category'}
                    value={'Home Services'}/>*/}
                    <Separator width={width - 50}/>
                    <DropDownWidget
                        onPress={() => {
                            this.setState({
                                displayScondaryContent: true
                            })
                        }}
                        actionTitle={'Type of Job'}
                        value={'Plumber'}/>
                    <Separator width={width - 50}/>
                </Group>

                <Group>
                    <Separator width={width - 50}/>
                    <EditableWidget
                        actionTitle={'Hourly Rate'}
                        keyboardType={'numeric'}
                        keyboardAppearance={'dark'}
                        value={'25'}/>
                    <Separator width={width - 50}/>
                </Group>

                <Group>
                    <Separator width={width - 50}/>
                    <BoolWidget
                        actionTitle={'Working remotely'}
                        value={false}/>
                    <Separator width={width - 50}/>
                    <BoolWidget
                        actionTitle={'Working from a place'}
                        value={false}/>
                    <Separator width={width - 50}/>
                    <BoolWidget
                        actionTitle={'Working from a home'}
                        value={false}/>
                    <Separator width={width - 50}/>
                </Group>

                <Group>
                    <Separator width={width - 50}/>
                    <DropDownWidget
                        actionTitle={'Days available to work'}
                        value={'Week Days'}/>
                    <Separator width={width - 50}/>
                    <DropDownWidget
                        actionTitle={'Hours available to work'}
                        value={'Plumber'}/>
                    <Separator width={width - 50}/>
                </Group>

            </BottomSheet>
        )
    }


    renderCategories(height, width) {
        if (this.props.isCategoriesVisible) {

            let makeListOfCategories = () => {
                let list = this.props.categoriesList;
                // console.log(this.props);
                return list.map(name => {
                    return <View key={shortid.generate()}>
                        {this.makeWidget(name)}
                        <Separator width={width - 50}/>
                    </View>
                })
            }

            return (
                <BottomSheet
                    ref={c => {this.bottomSheet = c;}}
                    transparent
                    native
                    height={300}
                    color={'rgba(43, 64, 46, 0.8)'}
                    visible={this.props.isCategoriesVisible}
                    setHidden={this.dismissCategories.bind(this)}
                    titleText={'Select a Category'}
                >
                    <Group>
                        {makeListOfCategories()}
                    </Group>
                </BottomSheet>
            )
        }
    }

    renderFilterBar(height, width) {
        return (
            <FilterBar
                onPress = {
                    () => {
                        let {dispatch} = AppStore.store;
                        dispatch(displayFilter());
                    }
                }
                width={width}/>
        )
    }

    onRefresh() {
        /// TODO add the option to refresh the list.
        //// Pedro just link the api here as you did in when the screen load.
        console.log('refreshing');
    }

    renderJobList() {
        return (
            <ListView
                refreshControl={
                    <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRefresh.bind(this)}
                />}
                dataSource={this.state.dataSource}
                renderRow={(data) =>
                    <Card {...data} onPress={()=> {
                        let {dispatch} = AppStore.store;
                        dispatch(displayJobDetails(data));
                    }}/>}
            />
        )
    }

    render() {
        let {width, height} = Dimensions.get('window')
        return (
            <View key={'main'} style={{height: height - 64, width: width, position: 'absolute', left: 0, bottom: 0, backgroundColor: '#F7F7F7'}}>
                {this.renderNewPost(height+20, width)}
                {this.renderFilter(height, width)}
                {this.renderCategories(height, width)}
                {this.renderFilterBar(height, width)}
                {this.renderJobList(height, width)}
                <ActionButton onPress={() => {
                    let {dispatch} = AppStore.store;
                    dispatch(displayNewPost());
                }}/>
            </View>
        )
    }
}

let jobsScreen = connect(
    state => ({ isNewPostVisible: state.homeScene.isNewPostVisible,
              isFilterVisible: state.homeScene.isFilterVisible,
              isCategoriesVisible: state.homeScene.isCategoriesVisible,
              jobPosts:  state.homeScene.jobPosts,
              activeCategory: state.categories.activeCategory,
              categoriesList: state.globalData.categoriesList,
          })
)(JobsScreen)

jobsScreen.props = JobsScreen.props;

export default jobsScreen;
