
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Navigator,
  Platform,
  TouchableOpacity,
  StatusBar,
  Modal,
  Picker,
  Dimensions,
  Animated,
  LayoutAnimation,
  ScrollView,
  TextInput,
  Image
} from 'react-native';

const DEV = false;

import {
    Form,
    Group,
    PhotoUpload,
    Separator,
    Label,
    ActionWidget } from '../../components/general';

import {
    displayRegistration
} from '../../store/actions';

/// Model
import AppStore from '../../store';

import Scene from '../../core/scene.js';
import Screen from '../../core/screen.js';
import { connect } from 'react-redux';

import {
    Page,
    PageViewer,
    BottomSheet
} from '../../components/';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    navbarItem: {
        height: (Platform.OS === 'ios') ? 44 : 56,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    navBar: {
        backgroundColor: '#808080',
        justifyContent: 'center',
        height: (Platform.OS === 'ios') ? 64 : 56 + 20,
    },
    Container: {
        flex: 1,
        justifyContent: 'center'
    },
    topContainer: {
        backgroundColor: '#2b402e',
        height: 50,
        width: width,
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    centerComp: {
        flex: 1,
        alignItems: 'center'
    },
    leftComp: {
        alignSelf: 'stretch',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    rightComp: {
        alignSelf: 'auto'
    },
    topImage: {
        margin: 6
    },
    topText: {
        color: 'white',
        fontSize: 20,
        marginRight: 55
    },
    topSmallText: {
        color: 'white',
        fontSize: 16
    },
    photo: {
        width: 90,
        height: 90,
        borderRadius: 45,
        backgroundColor: 'white'
    },
    textInput: {
        color: 'gray',
        width: width/1.17,
        alignSelf: 'stretch',
        marginLeft: 25,
        marginTop: 2,
        marginRight: 30,
        height: 35
    },
    form: {
        flex: 1,
        justifyContent: 'center'
    }
});

class RegistrationScreen extends Screen {
    constructor(props) {
        super(props);

        this.state = {
            language: null,
        }
    }

    static renderTitle (){
        return <View></View>
    }

    static renderLeftButton (){
        return <View></View>
    }

    static renderRightButton (){
        return <View></View>
    }

    render() {
        return(
            <View style={{flex:1}}>
                <View style={styles.topContainer} >
                    <TouchableOpacity style={styles.leftComp} >
                        <Image
                            source={require('../../components/general/images/arrow-back.png')}
                            style={styles.topImage}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.leftComp} >
                        <Text style={styles.topSmallText} >
                            Back
                        </Text>
                    </TouchableOpacity>
                    <View style={styles.centerComp} >
                        <Text style={styles.topText} >
                            Register
                        </Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.photoContainer} >
                        <PhotoUpload style={styles.photo} />
                    </View>
                    <View style={styles.form}>
                        <Form>
                            <Group>
                                <Separator width={width/1.2} style={{alignSelf: 'center'}} />
                                <Label name='FIRST NAME' style={{marginTop: 3, marginLeft: 30}} />
                                <TextInput
                                    keyboardType='default'
                                    underlineColorAndroid='gray'
                                    style={styles.textInput}
                                />
                                <Separator width={width/1.2} style={{alignSelf: 'center'}} />
                                <Label name='LAST NAME' style={{marginTop: 3, marginLeft: 30}} />
                                <TextInput
                                    keyboardType='default'
                                    underlineColorAndroid='gray'
                                    style={styles.textInput}
                                />
                                <Separator width={width/1.2} style={{alignSelf: 'center'}} />
                                <Label name='EMAIL' style={{marginTop: 3, marginLeft: 30}} />
                                <TextInput
                                    keyboardType='email-address'
                                    underlineColorAndroid='gray'
                                    style={styles.textInput}
                                />
                                <Separator width={width/1.2} style={{alignSelf: 'center'}} />
                            </Group>
                            <Group>
                                <Separator width={width/1.2} style={{alignSelf: 'center'}} />
                                <Label name='ADDRESS' style={{marginTop: 3, marginLeft: 30}} />
                                <TextInput
                                    keyboardType='default'
                                    underlineColorAndroid='gray'
                                    style={styles.textInput}
                                />
                                <Separator width={width/1.2} style={{alignSelf: 'center'}} />
                                <Label name='ZIP CODE' style={{marginTop: 3, marginLeft: 30}} />
                                <TextInput
                                    keyboardType='default'
                                    underlineColorAndroid='gray'
                                    style={styles.textInput}
                                />
                                <Separator width={width/1.2} style={{alignSelf: 'center'}} />
                            </Group>
                            <Group>
                                <Separator width={width/1.2}/>
                                <ActionWidget actionTitle={'Change Password'}/>
                                <Separator width={width/1.2}/>
                                <ActionWidget actionTitle={'Terms and Agreement'}/>
                                <Separator width={width/1.2}/>
                            </Group>
                        </Form>
                </View>
                </ScrollView>
            </View>
        );
    }
}

let registrationScreen = connect(
  state => ({ registration: state})
)(RegistrationScreen)

registrationScreen.props = RegistrationScreen.props;

export default RegistrationScreen;
