import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView
} from 'react-native';

export default class PageViewer extends Component {
    constructor(props) {
        super(props);

        this.pages = React.Children.map(this.props.children, elm => {
            return elm;
        });

        this.numberOfPages = this.pages.length;
        this.currentPage = 1;
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.currentPage !== prevProps.currentPage) {
            // console.log('different');
            // console.log(this.props.currentPage);
            this._scrollView.scrollTo({x: this.props.currentPage * this.props.width, y: 0, animated: true})
        }
    }

    render() {
        return (
            <ScrollView
                ref={(c) => {this._scrollView = c;}}
                {...this.props}
                horizontal
                pagingEnabled
                scrollEnabled={false}
                alwaysBounceHorizontal={false}
                bounces={false}
                showsHorizontalScrollIndicator={false}
            >
            </ScrollView>
        );
    }
}
