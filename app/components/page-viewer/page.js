import React, { Component } from 'react';
import {
  View,
  ScrollView
} from 'react-native';

export default class Page extends Component {
    render() {
        return (
            <ScrollView>
                <View
                    {...this.props}
                    style={[
                        {height: this.props.height, width: this.props.width},
                        this.props.style
                    ]}>
                    {this.props.children}
                </View>
            </ScrollView>
        )
    }
}
