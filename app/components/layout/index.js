import React, { Component } from 'react';
import {
  View,
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    transparent: {
        backgroundColor: 'transparent'
    },

    full: {
        flex: 1
    }
});

export class Container extends Component {
    /// The container will allow debugmode and some other tools.
    render() {
        return (
            <View style={{flex: 1}}>
                { this.props.children }
            </View>
        )
    }
}

export class Row extends Component {
    render() {
        return (
            <View style={styles.full}>
                { this.props.children }
            </View>
        )
    }
}

export const Empty = () => {
    return <View style={[styles.full, styles.transparent]}/>
}

export class Group extends Component {
    render() {
        return (
            <View style={[styles.full, {backgroundColor: '#F7F7F7'}]}>
                {this.props.children}
            </View>
        );
    }
}

export const Divider = () => {
    return <View style={{height: 1, backgroundColor: '#E4E4E4'}}/>
}
