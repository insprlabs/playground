
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView
} from 'react-native';

const {width, height} = Dimensions.get('window');
const radius = (Platform.OS === 'ios') ? 10 : 0;

import { Divider, Group, Empty } from '../layout'; /// Load layout helpers
import { PageViewer, Page } from '../page-viewer';

import Bar from './bar.js';

const styles = StyleSheet.create({
    bottomSheet: {
        maxHeight: height - 20,
        backgroundColor: 'white',
        borderTopLeftRadius: radius,
        borderTopRightRadius: radius,
        //iOS Shadow
        shadowColor: '#2B402E',
        shadowOpacity: 0.9,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 4,
        // Android Shadow
        elevation: 5,
    },

    background: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'rgba(0, 0, 0, 0.5)'
    },

    center: {
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default class BottomSheet extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentPage: 0,
            dist: new Animated.Value(0), // init opacity 0,
            height: 0,
        }

        this.visible = false;
        this.height = 0;
    }

    // shouldComponentUpdate() {
    //     return this.state.isTransition;
    // }

    componentWillUpdate(nextProps, nextState) {
        let displayScondaryContent  = this.props.displayScondaryContent;

        if (displayScondaryContent !== nextProps.displayScondaryContent) {

            if (!displayScondaryContent) {
                this.setState({currentPage: 1})
            } else {
                this.setState({currentPage: 0})
            }
        }

        // scrollTo
        // if (this.props.visible) {
        //     this.visible = true;
        //     this.displayModal();
        // } else {
        //     this.dismissModal(() => {
        //         this.visible = false;
        //     });
        // }
    }

    componentDidMount() {
            setTimeout(() => {
                this.displayModal();
            }, 350);
    }

    displayModal() {
            // this.setState({animationStarted: true});
            Animated.timing(                // Uses easing functions
                  this.state.dist,     // The value to drive
                  {
                      toValue: 1,
                      duration: (Platform.OS == 'ios') ? 350 : 450,
                    //   useNativeDriver: (Platform.OS == 'ios') ? this.props.native : false,
                  }              // Configuration
              ).start(()=> {
                  this.setState({
                      dist: new Animated.Value(1),
                      animationStarted: false
                  });
              });
    }

    dismissModal() {
        Animated.timing(                // Uses easing functions
              this.state.dist,     // The value to drive
              {
                  toValue: 0,
                  duration: (Platform.OS == 'ios')? 250 : 350,
              }              // Configuration
          ).start(()=>{
              this.setState({
                  dist: new Animated.Value(0),
              });
              this.props.setHidden();
          });
    }

    onPressRightButton() {
        this.dismissModal();
        if (this.props.righAction) {
            this.props.righAction();
        }
    }

    onPressLeftButton() {
        this.dismissModal();
        if (this.props.leftAction) {
            this.props.leftAction();
        }
    }

    onPressReturnButton() {

    }

    renderBar() {
        if (!this.props.displayScondaryContent) {
            return (
                <Bar
                    hasRight
                    hasLeft
                    onPressLeftButton={this.onPressLeftButton.bind(this)}
                    onPressRightButton={this.onPressRightButton.bind(this)}
                    rightButtonText={this.props.mainActionText}
                    titleText={this.props.titleText}
                />
            )
        } else {
            return (
                <Bar
                    hasLeft
                    backButton
                    onPressLeftButton={this.onPressLeftButton.bind(this)}
                    onPressRightButton={this.onPressRightButton.bind(this)}
                    onPressReturnButton={this.props.onDismissSecondaryContent}
                    rightButtonText={"return"}
                    titleText={'Select ...'}
                />
            )
        }


    }

    render() {
        const transformStyle = {
            transform:[
                 {
                     translateY: this.state.dist.interpolate({
                       inputRange: [0, 1],
                       outputRange: [this.props.height, 0]
                     })
                 }
            ]
        }


        // renderTheContent() {
        //     if (this.props.renderSecundaryScreen) {
        //
        //     }
        // }

        return (
            <Modal
                {...this.props}
                onRequestClose={()=>{}}
                animationType={'fade'}
            >
                <View style={
                    [styles.background,
                    (this.props.color) ? {backgroundColor: this.props.color} : null]
                }>

                    <Empty/>
                    <Animated.View
                        style={[styles.bottomSheet, transformStyle]}>

                        {this.renderBar()}

                        <Divider/>
                        <Group>
                            <PageViewer
                                width={width} // The width of the page
                                currentPage={this.state.currentPage}
                            >
                                {/*//Render the main contend*/}

                                <Page
                                    width={width}
                                    height={this.props.height}
                                    style={[{backgroundColor: 'transparent'}]}>
                                    {/*<TouchableOpacity onPress={() => {
                                        this.setState({currentPage: 1})
                                        }}>
                                        <Text>NextPage</Text>
                                    </TouchableOpacity>*/}

                                    {this.props.children}
                                </Page>

                                {/*// Render the options, used to secundary actions*/}
                                <Page
                                    width={width}
                                    height={this.props.height}
                                    style={[{backgroundColor: 'transparent'}]}>
                                    {this.props.secundaryContent}
                                </Page>
                            </PageViewer>
                        </Group>
                    </Animated.View>
                </View>
            </Modal>
        );
    }
}
