import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Dimensions,
  Animated,
  Image,
} from 'react-native';

const styles = StyleSheet.create({
    bar: {
        height: 36,
        backgroundColor: 'transparent',
        flexDirection: 'row'
    },

    center: {
        justifyContent: 'center',
        alignItems: 'center'
    }
});


class BarBackButton extends Component {
    render() {
        if (Platform.OS == 'android') {
            return (
                <TouchableOpacity {...this.props} style={{height: 32, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                        source={
                            require('../general/arrow-back-android-green/arrow-back-android-green.png')
                        }
                        style={{width: 19, height: 32}}>
                    </Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity {...this.props} style={{height: 32, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                        source={require('../general/arrow-back-green/arrow-back-green.png')}
                        style={{width: 11, height: 32}}>
                    </Image>

                    <Text style={{color: '#27976D', fontSize: 16, marginLeft: 6}}>Back</Text>
                </TouchableOpacity>
            )
        }
    }
}

export default class Bar extends Component {


    renderLeftButton() {

        if (this.props.hasLeft) {
            if (this.props.backButton) {
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', padding: 15, width: 80}}>

                        <BarBackButton onPress={this.props.onPressReturnButton}/>

                    </View>
                )
            } else {
                return (
                    <View style={{justifyContent: 'center', alignItems: 'center', padding: 15, width: 80}}>

                        <TouchableOpacity onPress={() => {
                            this.props.onPressRightButton();
                        }}>
                            <Text style={{color: 'rgba(255,0,0, 0.8)'}}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                )
            }

        } else {
            return (
                <View style={{justifyContent: 'center', alignItems: 'center', padding: 15, width: 80}}>

                </View>
            )
        }
    }

    renderRightButton() {
        if (this.props.hasRight) {
            return (
                <View style={{justifyContent: 'center', alignItems: 'center', padding: 15, width: 80}}>
                    <TouchableOpacity onPress={() => {
                        this.props.onPressRightButton();
                    }}>
                        <Text style={{color: '#27976D'}}>{this.props.rightButtonText}</Text>
                    </TouchableOpacity>
                </View>
            )
        } else {
            return (
                <View style={{justifyContent: 'center', alignItems: 'center', padding: 15, width: 80}}>

                </View>
            )
        }
    }

    render() {
        return (
            <View style={styles.bar}>

                {this.renderLeftButton()}


                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{color: 'black'}}>{this.props.titleText}</Text>
                </View>

                {this.renderRightButton()}
            </View>
        )
    }
}
