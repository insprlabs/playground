import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  Image,
  TextInput,
  Switch
} from 'react-native';

import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export class Label extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: ''
        }
    }
    render() {
        return(
            <Text style={{
                fontSize: 12,
                color: '#AAAAAA'
            }} >
                {this.props.name}
            </Text>
        );
    }
}

export class ImageUploadWidget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            isSelected: false,
            image: null
        }
    }

    handlePress() {
        let options = {
          title: 'Select an image',
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };

        ImagePicker.showImagePicker(options, (response) => {
        // console.log('Response = ', response);

        if (response.didCancel) {
            console.log('User cancelled image picker');
        }
        else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
        }
        else {
            // // You can display the image using either data...
            // const source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

            // or a reference to the platform specific asset location
            if (Platform.OS === 'ios') {
                const source = {uri: response.uri.replace('file://', ''), isStatic: true};
                this.setState({
                    image: source,
                    isSelected: true,
                });

                this.props.onSelectImage(source);

            } else {
                const source = {uri: response.uri, isStatic: true};
                this.setState({
                    image: source,
                    isSelected: true,
                });

                this.props.onSelectImage(source);
            }
        }
    });
    }

    renderImage() {
        if (this.state.isSelected) {
            return <Image
                style={{
                    width: 78,
                    height: 78,
                    borderRadius: 4
                }}
                source={this.state.image}/>
        } else {
            return <Image
                style={{
                    width: 78,
                    height: 78,
                    borderRadius: 4
                }}
                source={require('./images/add/add.png')}/>
        }
    }

    render() {
        return (
            <TouchableOpacity
                {...this.props}
                onPress={this.handlePress.bind(this)}
                style={{
                    width: 78,
                    height: 78,
                    backgroundColor: 'rgba(43, 64, 46, 0.2)',
                    borderRadius: 4,
                    marginLeft: 10,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                {this.renderImage()}
            </TouchableOpacity>
            );
    }
}
/*
export class Button extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            color: '',
            source: ' ',
            fontColor: ''
        }
    }
    renderText() {
        return (
            <Text style={{color: this.props.fontColor,
            fontSize: 18,
            marginTop: 15,
            marginBottom: 15}} >
                {this.props.name}
            </Text>
        );
    }
    renderImage() {
        if(this.props.source !== ' ') {
            return (
                <Image source={this.props.source}
                style={{width: 35, height: 35}} />
            );
        }
    }
    render() {
        return(
            <View style={{backgroundColor: this.props.color,
            flexDirection: 'row',
            marginTop: 2,
            marginBottom: 3}} >
                <View style={{justifyContent: 'center', alignItems: 'flex-start'}} >
                    {this.renderImage()}
                </View>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                    {this.renderText()}
                </View>
            </View>
        );
    }
}*/

export class PhotoUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noImage: true,
            camera: require('./images/camera-plus.png'),
            nameImage: ''
        }
    }
    swapImage() {
        let {noImage} = this.state;

        this.setState({
            noImage: !noImage,
        })
    }
    renderPhoto() {
        if(!this.state.noImage){
            return (
                <View style={{width: width/3, height: 100, backgroundColor: 'red', borderRadius: 50}} ></View>
            );
        } else {
            return(
                <Image source={this.state.camera} />
            );
        }
    }
    render() {
        return (
            <TouchableOpacity style={{
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 20,
            padding: 15,
            height: 200
            }}
            onPress={this.swapImage.bind(this)}>
                {this.renderPhoto()}
            </TouchableOpacity>
        );
    }
}

export class Form extends Component {
    render() {
        return (
            <KeyboardAwareScrollView style={{flex: 1}}>
                { this.props.children }
            </KeyboardAwareScrollView>
        )
    }
}

export class Group extends Component {
    render() {
        return (
            <View
                style={{
                    backgroundColor: 'rgba(216, 216, 216, 0.05)',
                    marginTop: 5,
                    marginBottom: 5,
                }}
                {...this.props}
            >
                { this.props.children }
            </View>
        )
    }
}

export class Separator extends Component {
    render() {
        return (
            <View style={{
                width: this.props.width,
                backgroundColor: 'rgba(0, 0, 0, 0.05)',
                height: 1,
                alignSelf: 'flex-end'
            }}/>
        )
    }
}

// export class TitleInput extends Component {
//     render() {
//         return (
//             <View style={{width: this.props.width, padding: 10, paddingBottom: 0}}>
//
//             <Text
//             style={{
//                 marginLeft: 10,
//                 marginBottom: 5,
//                 fontSize: 10,
//                 fontWeight: 'bold',
//                 color: 'rgba(108, 131, 110, 0.5)'
//             }}>JOB TITLE</Text>
//
//             <TextInput
//             maxLength={50}
//             underlineColorAndroid={'white'}
//             returnKeyType='done'
//             keyboardAppearance={'dark'}
//             style={{
//                 minHeight: 30,
//                 width: this.props.width-30,
//                 marginLeft: 10,
//                 fontWeight: 'bold',
//                 fontSize: 17,
//             }}
//             placeholder={'Enter Job Title'}/>
//             </View>
//         )
//     }
// }

export class TitleInput extends Component {
    constructor(props) {
        super(props);

        this.state = {text: '', height: (Platform.OS == 'android') ? 40 : 25.5};
        this.layout = null;
    }

    renderCounter() {
            return (
                <View
                    style={{position: 'absolute', right: 10, top: 10, height: 10, alignItems: 'flex-end'}}>
                    <Text style={{
                        fontSize: 11,
                        fontWeight: 'bold',
                        color: this.state.text.length <= (this.props.maxLength - 20) ? 'black': 'red'
                    }}>{this.props.maxLength - this.state.text.length}</Text>
                </View>
            );
    }

    render() {
        return (
            <View style={{width: this.props.width, padding: 10, paddingBottom: 10}}>
            {this.renderCounter()}
            <Text
            style={{
                marginLeft: 10,
                marginBottom: 10,
                fontSize: 10,
                fontWeight: 'bold',
                color: 'rgba(108, 131, 110, 0.5)'
            }}>JOB TITLE</Text>

            <TextInput
            maxLength={this.props.maxLength}
            multiline={true}
            underlineColorAndroid={'transparent'}
            returnKeyType='done'
            keyboardAppearance={'dark'}
            style={
                {
                    minHeight: 30,
                    marginLeft: 10,
                    height: this.state.height,
                    width: this.props.width-30,
                    fontSize: 20,
                    fontWeight: 'bold'
                }
            }
            onChange={(event) => {
                console.log(event.nativeEvent.contentSize.height);
              this.setState({
                text: event.nativeEvent.text,
                height: event.nativeEvent.contentSize.height,
              });
            }}
            value={this.state.text}
            placeholder={'Enter Job Title'}/>
            </View>
        )
    }
}

export class JobTitle extends Component {
    constructor(props) {
        super(props);

        this.state = {text: '', height: 22};
        this.layout = null;
    }

    render() {
        return (
            <View style={{width: this.props.width, padding: 10, paddingBottom: 10}}>
            <Text
            style={{
                marginLeft: 10,
                marginBottom: 10,
                fontSize: 10,
                fontWeight: 'bold',
                color: 'rgba(108, 131, 110, 0.5)'
            }}>JOB TITLE</Text>

            <Text
            multiline={true}
            style={
                {
                    minHeight: 30,
                    marginLeft: 10,
                    width: this.props.width-30,
                    fontSize: 20,
                    fontWeight: 'bold'
                }
            }>{this.props.value}</Text>
            </View>
        )
    }
}

export class JobDescription extends Component {
    constructor(props) {
        super(props);

        this.state = {text: '', height: 22};
        this.layout = null;
    }

    render() {
        return (
            <View style={{width: this.props.width, padding: 10, paddingBottom: 10}}>
            <Text
            style={{
                marginLeft: 10,
                marginBottom: 10,
                fontSize: 10,
                fontWeight: 'bold',
                color: 'rgba(108, 131, 110, 0.5)'
            }}>JOB DESCRIPTION</Text>

            <Text
            multiline={true}
            style={
                {
                    minHeight: 30,
                    marginLeft: 10,
                    width: this.props.width-30,
                    fontSize: 14,
                }
            }>{this.props.value}</Text>
            </View>
        )
    }
}

export class DescriptionInput extends Component {
    constructor(props) {
        super(props);

        this.state = {text: '', height: (Platform.OS == 'android') ? 35 : 22};
        this.layout = null;
    }

    renderCounter() {
            return (
                <View
                    style={{position: 'absolute', right: 10, top: 10, height: 10, alignItems: 'flex-end'}}>
                    <Text style={{
                        fontSize: 11,
                        fontWeight: 'bold',
                        color: this.state.text.length <= (this.props.maxLength - 20) ? 'black': 'red'
                    }}>{this.props.maxLength - this.state.text.length}</Text>
                </View>
            );
    }

    render() {
        return (
            <View style={{width: this.props.width, padding: 10, paddingBottom: 10}}>
            {this.renderCounter()}
            <Text
            style={{
                marginLeft: 10,
                marginBottom: 10,
                fontSize: 10,
                fontWeight: 'bold',
                color: 'rgba(108, 131, 110, 0.5)'
            }}>JOB DESCRIPTION</Text>

            <TextInput
            maxLength={this.props.maxLength}
            multiline={true}
            underlineColorAndroid={'transparent'}
            keyboardAppearance={'dark'}
            style={
                {
                    minHeight: 30,
                    marginLeft: 10,
                    height: this.state.height,
                    width: this.props.width-30,
                    fontSize: 14,
                }
            }
            onChange={(event) => {
                console.log(event.nativeEvent.contentSize.height);
              this.setState({
                text: event.nativeEvent.text,
                height: event.nativeEvent.contentSize.height,
              });
            }}
            value={this.state.text}
            placeholder={'Enter Job Description'}/>
            </View>
        )
    }
}

export class Widget extends Component {
    render() {
        return (
            <View style={{
                width: this.props.width,
                height: 40,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
            }}>
            <View style={{
                height: 40,
                width: 20,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'flex-start',
            }}></View>

            <TouchableOpacity
            {...this.props}
            style={{
                width: this.props.width,
                height: 40,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
            }}>
            </TouchableOpacity>
            </View>
        )
    }
}

export class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: ''
        }
    }
    renderImage() {
        if (this.props.source){
            return (
                <Image source={this.props.source}
                    style={{ position: 'absolute', width: 20, height: 20, left: 10, top: 10}} />
            );
        }
    }
    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={{
                    flex: 1,
                    backgroundColor: this.props.color,
                    borderRadius: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row'
                }}>

                {this.renderImage()}

                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                    <Text style={{color: this.props.textColor, fontSize:14}}>{this.props.text}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

export class CheckWidget extends Component {
    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={{
                    width: this.props.width,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <View style={{
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    left: 0,
                }}>
                    <Image
                        source={ (this.props.active) ? require('./images/mark.png') : null}
                        style={{width: 11, height: 32, marginLeft: 15, marginRight: 15}}>
                    </Image>
                </View>

                { /*<View style={{
                    height: 40,
                    width: 20,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}></View>*/ }

                <View
                    {...this.props}
                    style={{
                        width: this.props.width,
                    marginLeft: 40,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <Text>{this.props.actionTitle}</Text>
                </View>
                </TouchableOpacity>
            )
    }
}


export class ActionWidget extends Component {
    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={{
                    width: this.props.width,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <View style={{
                    height: 40,
                    width: 20,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}></View>

                <View
                    {...this.props}
                    style={{
                        width: this.props.width,
                        height: 40,
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center'
                    }}>
                    <Text>{this.props.actionTitle}</Text>
                </View>
                <View style={{
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    right: 0,
                }}>
                    <Image
                        source={require('./images/arrow-next.png')}
                        style={{width: 11, height: 32, marginLeft: 15, marginRight: 15}}>
                </Image>
                </View>
                </TouchableOpacity>
            )
    }
}

export class DropDownWidget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: true
        }
    }

    renderValue() {
        if (this.props.value) {
            return (
                <Text style={{color: 'rgb(43, 64, 46)', paddingLeft: 10, marginLeft: (Platform.OS == 'android') ? 10 : 0 }}>{this.props.value}</Text>
            )
        }
    }

    renderImage() {
        if (!this.props.inactive) {
            return <Image
                source={require('./images/arrow-next.png')}
                style={{width: 11, height: 32, position: 'absolute', top: 5, right: 15}}/>
        }
    }

    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={{
                    width: this.props.width,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <View style={{
                    height: 40,
                    width: 20,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}></View>

                <Text style={{color: 'rgba(43, 64, 46, 0.5)'}}>{this.props.actionTitle}</Text>

                {this.renderValue()}
                {this.renderImage()}

                </TouchableOpacity>
            )
    }
}


export class BoolWidget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: true
        }
    }

    renderValue() {
        if (this.props.value) {
            return (
                <Text style={{color: 'rgb(43, 64, 46)', paddingLeft: 10}}>{this.props.value}</Text>
            )
        }
    }

    renderSwitch() {
        if (this.props.incative) {
            return <Text style={{
                color: 'rgb(43, 64, 46)',
                paddingLeft: 10,
            }}>{this.props.value ? "YES" : "NO"}</Text>
        } else {
            return <Switch
            value={this.props.value}
            style={{width: 11, height: 32, position: 'absolute', top: 5, right: 50}}/>
        }
    }

    render() {
        return (
            <View
                {...this.props}
                style={{
                    width: this.props.width,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <View style={{
                    height: 40,
                    width: 20,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}></View>

                <Text style={{color: 'rgba(43, 64, 46, 0.5)'}}>{this.props.actionTitle}</Text>

                {/*{this.renderValue()}*/}
                {this.renderSwitch()}
                </View>
            )
    }
}



export class EditableWidget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: true
        }
    }

    renderAction() {
        if (this.state.editMode) {
            return <TextInput {...this.props}
                style={{flex: 1, height: 40, fontSize: 14, paddingLeft: 10}}
                placeHolder={this.props.actionTitle}/>
        }
    }

    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={{
                    width: this.props.width,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <View style={{
                    height: 40,
                    width: 20,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}></View>

                <Text style={{color: 'rgba(43, 64, 46, 0.5)'}}>{this.props.actionTitle}</Text>
                {this.renderAction()}

                </TouchableOpacity>
            )
    }
}

export class InputWidget extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: true
        }
    }

    renderAction() {
        if (this.state.editMode) {
            return <TextInput {...this.props}
                style={{flex: 1, height: 40, fontSize: 14, paddingLeft: 10}}
                placeHolder={this.props.actionTitle}/>
        }
    }

    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={{
                    width: this.props.width,
                    height: 40,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center'
                }}>
                <View style={{
                    height: 40,
                    width: 20,
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                }}></View>

                <Text style={{color: 'rgba(43, 64, 46, 0.5)'}}>{this.props.actionTitle}</Text>
                {this.renderAction()}

            </TouchableOpacity>
            )
    }
}

export class UserProfile extends Component {
    render() {
        return (
            <TouchableOpacity {...this.props}>
            <Image

            source={{uri:'https://pbs.twimg.com/profile_images/3765824192/dc0b71e8e69ba8ecee4c9bd261049512_400x400.jpeg'}}
            style={{width: 30, height: 30, borderRadius: 15, backgroundColor: '#2B402E'}}>
            </Image>
            </TouchableOpacity>
        )
    }
}

export class SearchButton extends Component {
    render() {
        return (
            <TouchableOpacity {...this.props} style={{width: 32, height: 32, justifyContent: 'flex-end', flexDirection: 'row'}}>
                <Image
                    source={require('./images/search-icon.png')}
                    style={{width: 22, height: 32}}>
            </Image>
            </TouchableOpacity>
        )
    }
}


export class FilterBar extends Component {
    render() {
        return (
            <TouchableOpacity
                {...this.props}
                style={
                    {height: 40, width: this.props.width, backgroundColor: 'white', borderBottomWidth: 1, borderColor: 'rgba(0, 0, 0, 0.1)', justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                <Image
                    source={require('./images/filter.png')}
                    style={{width: 25, height: 32, marginLeft: 10, marginRight: 10}}
                />
                <View style={{flex: 1, padding: 10}}>
                    <Text
                        numberOfLines={1}
                        style={{fontSize: 15, color: 'rgba(43, 64, 46, 0.5)'}}>Filter the posts by location, price, distance </Text>
                </View>


            </TouchableOpacity>
        )
    }
}

export class ActionButton extends Component {
    render() {
        return (
            <TouchableOpacity {...this.props} style={{height: 80, width: 80, justifyContent: 'center', alignItems: 'center', position: 'absolute', right: 0, bottom: (Platform.OS == 'android') ? 10 : 0}}>

                <Image
                    source={require('./action-button/action-button.png')}
                >

                </Image>

            </TouchableOpacity>
        )
    }
}

export class BackButton extends Component {
    render() {
        if (Platform.OS == 'android') {
            return (
                <TouchableOpacity {...this.props} style={{height: 32, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                        source={require('./images/arrow-back-android.png')}
                        style={{width: 19, height: 32}}>
                    </Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity {...this.props} style={{height: 32, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                        source={require('./images/arrow-back.png')}
                        style={{width: 11, height: 32}}>
                </Image>

                <Text style={{color: 'white', fontSize: 16, marginLeft: 6}}>Back</Text>
                </TouchableOpacity>
            )
        }
    }
}

export class Title extends Component {
    render() {
        return (
            <Text style={{color: 'white', fontSize: 17, fontWeight: '600'}}>{this.props.text}</Text>
        )
    }
}

export class DropDown extends Component {
    render() {
        return (
            <TouchableOpacity {...this.props} style={{height: 32, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'}}>

                <Text style={{color: 'white', fontSize: 17, fontWeight: '600'}}>{this.props.text}</Text>
                <Image
                    source={require('./images/arrow-down.png')}
                    style={{width: 13, height: 32, marginLeft: 6}}>
                </Image>
            </TouchableOpacity>
        )
    }
}

class DropDownTitle extends Component {
    render() {
        return (
            <TouchableOpacity {...this.props} style={{height: 32, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'}}>

                <Text style={{color: 'white', fontSize: 17, fontWeight: '600'}}>{this.props.activeCategory}</Text>
                <Image
                    source={require('./images/arrow-down.png')}
                    style={{width: 13, height: 32, marginLeft: 6}}>
                </Image>
            </TouchableOpacity>
        )
    }
}

let dropdown = connect(
  state => ({ isNewPostVisible: state.homeScene.isNewPostVisible,
              isFilterVisible: state.homeScene.isFilterVisible,
              isCategoriesVisible: state.homeScene.isCategoriesVisible,
              jobPosts:  state.homeScene.jobPosts,
              activeCategory: state.categories.activeCategory
          })
)(DropDownTitle)

export {dropdown as DropDownTitle};
