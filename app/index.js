import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  Platform,
  TouchableOpacity,
  StatusBar,
  Modal,
  Picker,
  Dimensions,
  Animated,
  LayoutAnimation,
  ScrollView,
  Easing
} from 'react-native';

const DEV = false;

import { Router, Screen } from './core';
import { MainScene, AuthScene } from './layout/scenes';
import {
    JobsScreen,
    ProfileScreen,
    SearchScreen,
    JobDetailsScreen,
    LoginScreen,
    RegistrationScreen,
    SplashScreen
} from './layout/screens';
/// Model

import {GetCashAPI} from './services'
global.api = new GetCashAPI

import AppStore from './store';

import {
    rootReducer,
    loadState,
    saveState
} from './store';

import { applyMiddleware, createStore } from 'redux';
import createLogger from 'redux-logger';

import { connect } from 'react-redux';

import {
    Page,
    PageViewer,
    BottomSheet
} from './components/';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    navbarItem: {
        height: (Platform.OS === 'ios') ? 44 : 56,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },

    navBar: {
        backgroundColor: '#808080',
        justifyContent: 'center',
        height: (Platform.OS === 'ios') ? 64 : 56 + 20,
    }
});


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            language: null,
        }
    }

    renderMainScene() {
        return(
            <MainScene key="HomeScene">
                <Screen
                    {...JobsScreen.props()}
                    key="MainScreen"
                    dispatch={this.props.dispatch}
                    content={<JobsScreen/>}
                />

                <Screen
                    {...ProfileScreen.props()}
                    key="ProfileScreen"
                    dispatch={this.props.dispatch}
                    content={<ProfileScreen/>}
                />

                <Screen
                    {...SearchScreen.props()}
                    key="SearchScreen"
                    dispatch={this.props.dispatch}
                    content={<SearchScreen/>}
                />

                <Screen
                    {...JobDetailsScreen.props()}
                    key="JobDetailsScreen"
                    dispatch={this.props.dispatch}
                    content={<JobDetailsScreen/>}
                />
            </MainScene>
        );
    }

    renderAuthScene() {
        return(
            <AuthScene key="AuthScene">
                <Screen
                    {...LoginScreen.props()}
                    key="LoginScreen"
                    content={<LoginScreen/>}
                />
                {/*<Screen
                    {...RegistrationScreen.props()}
                    key="RegistrationScreen"
                    content={<RegistrationScreen/>}
                />*/}
            </AuthScene>
        );
    }

    renderScene() {
        // const isAuthenticated = true;
        // console.log(this.props);
        if (!this.props.isAuthenticated) {
            return this.renderAuthScene();
        } else {
            return this.renderMainScene();
        }
    }

    render() {
        return (
            <Router store={this.props.store}>
                <View>
                    <StatusBar
                        backgroundColor="#142516"
                        barStyle="light-content"
                        translucent={false}
                    />
                    {this.renderScene()}
                </View>

            </Router>
        );
    }
}

let Application = connect(
  state => ({
          isAuthenticated: state.globalData.isAuthenticated,
            //   isFilterVisible: state.homeScene.isFilterVisible,
            //   isCategoriesVisible: state.homeScene.isCategoriesVisible,
            //   jobPosts:  state.homeScene.jobPosts,
            //   activeCategory: state.categories.activeCategory
        })
)(App)

export default  class Root extends Component {
    constructor(props) {
        super(props);

        // const initialState = {
        //     isLoading: true,
        // }
        //
        // const tempReducer = (state=initialState, action) => {
        //     if (typeof state === 'undefined') {
        //       return initialState
        //     }
        // }

        this.state = {
            isLoading: true,
            fadeAnim: new Animated.Value(1), // init opacity 0
        }
    }

    componentDidMount() {

        const store = createStore(
          rootReducer,
          // applyMiddleware(logger)
        );

        loadState().then(data => {
            console.log(data);
        })

        const logger = createLogger();

        AppStore.store = store;

        this.setState({fade: true, isTransitioning: true});

        setTimeout(() => {
            this.setState({store, isLoading: false});
            Animated.timing(          // Uses easing functions
            this.state.fadeAnim,    // The value to drive
            {
                toValue: 0,
                easing:  Easing.in(Easing.ease)
            }            // Configuration
        ).start(() => {
            this.setState({isTransitioning: false})
        });                // Don't forget start!
        }, 1000);

        // store.subscribe(() => {
        //     saveState(store.getState());
        // });
    }

    renderTransitionView() {
        if (this.state.isTransitioning) {
            return (
                <Animated.View style={{
                    backgroundColor: 'white',
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    opacity: this.state.fadeAnim
                }}>
                    <SplashScreen/>
                </Animated.View>
            );
        }
    }

    renderSplashScreen() {
        if (this.state.isLoading) {
            /// Add the splashscreen here.
            return <View style={{flex: 1, backgroundColor: 'white'}}>
                <SplashScreen/>
            </View>
        } else {
            return <View>
                <Router store={this.state.store}>
                    <Application
                        dispatch={this.state.store.dispatch}
                        store={this.state.store}/>
                </Router>
                
                {this.renderTransitionView()}
            </View>

        }
    }

    render() {
        return this.renderSplashScreen();
    }
}
