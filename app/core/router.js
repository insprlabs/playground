import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  StatusBar
} from 'react-native';

import { Provider } from 'react-redux';

export default class Router extends Component {
    render() {
        return (
            <Provider store={this.props.store}>
                <View style={{flex:1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
                    {/*<StatusBar
                        barStyle="light-content"
                    />*/}
                    {this.props.children}
                </View>
            </Provider>
        )
    }
}
