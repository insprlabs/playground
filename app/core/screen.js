import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  Navigator
} from 'react-native';

export default class Screen extends Component {
    constructor(props) {
        super(props);
    }

    static renderTitle() {
        throw new Error('Calling abstract function :: renderTitle')
    }

    static renderLeftButton() {
        throw new Error('Calling abstract function :: renderLeftButton')
    }

    static props() {
        return {
            title: this.renderTitle(),
            leftButton: this.renderLeftButton(),
            rightButton: this.renderRightButton()
        }
    }

    render() {
        return this.props.content
    }
}
