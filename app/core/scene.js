import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Modal,
  Dimensions,
  Animated,
  ScrollView,
  Navigator
} from 'react-native';

export default class Scene extends Component {
    constructor(props) {
        super(props);

        this.routes = React.Children.map(this.props.children, (elm, index) => {
            return {
                title: elm.props.title,
                leftButton: elm.props.leftButton,
                rightButton: elm.props.rightButton,
                screen: elm,
                index: index
                // leftButton: elm.props.renderLeftButton(),
                // rightButton: elm.props.renderRightButton()
            }
        });

        // console.log(this.routes);

        this.navigator= null;
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.activeRoute !== prevProps.activeRoutee) {
            console.log('changed route');

            if (this.props.activeRoute > prevProps.activeRoute) {
                this.navigator.push(this.routes[this.props.activeRoute]);
            } else {
                this.navigator.pop();
            }
        }
    }

    hasNavigationBar() {
        return false;
    }

    onDidFocus() {

    }

    onWillFocus(){

    }

    onWillLostFocus() {

    }

    onLostFocus(){

    }

    navBarStyle() {

    }

    _transformItem(item) {
        /// Return a bar item

        return (route, navigator, index, navState) => {
            return <View style={{height: (Platform.OS == 'android') ? 68 : 44, padding: 10, justifyContent: 'center', alignItems: 'center'}}>
                {item(route, navigator, index, navState)}
            </View>
        }
    }

    renderTitle(screen, navigator, index, navState) {
        return (screen.title)
    }

    renderLeftButton(screen, navigator, index, navState) {
        return (screen.leftButton);
    }

    renderRightButton(screen, navigator, index, navState) {
        return (screen.rightButton);
    }

    // renderScene() {
    //     <View {...this.props}
    //     style={[{flex:1, alignItems: 'center', justifyContent: 'center'}, this.props.style]}>
    //         {this.renderScene()}
    //     </View>
    // }

    renderNavigationBar() {
        if (this.hasNavigationBar()) {
            return (
                <Navigator.NavigationBar
                   routeMapper={{
                     LeftButton: this._transformItem(this.renderLeftButton.bind(this)),
                     RightButton: this._transformItem(this.renderRightButton.bind(this)),
                     Title: this._transformItem(this.renderTitle.bind(this))}}
                   style={this.navBarStyle()}
                 />
            )
        }
    }

    render() {
        // console.log(this.hasNavigationBar());
        const {width, height} = Dimensions.get('window');

        return (
            <Navigator
                style={{width: width, height: height}}
                initialRoute={this.routes[this.props.activeRoute]}
                initialRouteStack={this.routes}
                navigationBar={this.renderNavigationBar()}
                configureScene={(route, routeStack) =>
                Navigator.SceneConfigs.FloatFromRight}

                renderScene={(route, navigator) => {
                    this.navigator = navigator;
                    return (
                        <View {...this.props}
                        style={[{height: height, width: width, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white'}, this.props.style]}>

                        { route.screen }

                        {/*<TouchableOpacity onPress={() => {
                            if (route.index === 0) {
                            console.log(this.routes);
                        navigator.push(this.routes[1]);
                      } else {
                        navigator.pop();
                      }
                    }}>
                    <Text>Hello {route.title}!</Text>
                    </TouchableOpacity>*/}

                    </View>)
                    }
                  }
            />
        )
    }
}
