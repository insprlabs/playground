// export { applyMiddleware, createStore } from 'redux';
import rootReducer from './reducers';
// export createLogger from 'redux-logger';

import { loadState, saveState } from './localstorage.js';

export {rootReducer};
export {loadState, saveState};

let Store = {};
export default Store;
