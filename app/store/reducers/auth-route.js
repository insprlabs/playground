const initialState = {
    activeRoute: 0
}

export default function authScene(state = initialState, action) {
    if (typeof state === 'undefined') {
      return initialState
    }

  switch (action.type) {
    case 'LOGIN':
        return Object.assign({}, state, {
            activeRoute: 0
        });
    case 'REGISTRATE_NEW_USER':
        return Object.assign({}, state, {
            activeRoute: 1
        });
    default:
      return state
  }
}
