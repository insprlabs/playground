const initialState = {
    firstName: 'Breno',
    lastName: 'Rdr.',
    city: 'Boston',
    state: 'MA',
    zipcode: '234234'
}

export default function user(state=initialState, action) {
    if (typeof state === 'undefined') {
      return initialState
    }

    switch (action.type) {
      case 'UPDATE_PROFILE':
          return Object.assign({}, state, action.payload.userProfile);
      break;

      default: return state
    }
}
