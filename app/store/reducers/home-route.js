const initialState = {
    activeRoute: 0,
    isNewPostVisible: false,
    isCategoriesVisible: false,
    isFilterVisible: false,
    jobDetails: { title: 'Corporate Interactions Associate',
        value: 237,
        name: 'Pierce',
        zipcode: '90521',
        location: 'Smithamport',
        date: 'Aug 07',
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/kylefoundry/128.jpg',
        description: 'Facere minima repellendus consectetur nobis. Incidunt ipsum commodi doloribus ut et molestiae. Occaecati non mollitia non nesciunt.'
    },
}

export default function homeScene(state = initialState, action) {
    if (typeof state === 'undefined') {
      return initialState
    }

  switch (action.type) {
    case 'DISPLAY_HOME_SCREEN':
      return Object.assign({}, state, {
        activeRoute: 0
      })
      break;

    case 'DISPLAY_PROFILE_SCREEN':
        return Object.assign({}, state, {
            activeRoute: 1
        });
        break;

    case 'DISPLAY_SEARCH_SCREEN':
        return Object.assign({}, state, {
            activeRoute: 2
        });
        break;

    case 'DISPLAY_DETAILS':
        return Object.assign({}, state, {
            activeRoute: 3,
            jobDetails: action.payload.jobDetails
        });
        break;

    case 'DISPLAY_NEW_POST':
        // state.isNewPostVisible = true;
        // return state;
        return Object.assign({}, state, {
            isNewPostVisible: true
        });
        break;

    case 'HIDE_NEW_POST':
        return Object.assign({}, state, {
            isNewPostVisible: false
        });
        break;

    case 'DISPLAY_FILTER':
        return Object.assign({}, state, {
            isFilterVisible: true
        });
        break;

    case 'HIDE_FILTER':
        return Object.assign({}, state, {
            isFilterVisible: false
        });

    case 'DISPLAY_CATEGORIES':
        return Object.assign({}, state, {
            isCategoriesVisible: true
        });
        break;

    case 'HIDE_CATEGORIES':
        return Object.assign({}, state, {
            isCategoriesVisible: false
        });
        break;

    default:
      return state
      break;

  }
}
