const initialState = {
    activeCategory: 'Featured'
}

export default function categories(state=initialState, action) {
    if (typeof state === 'undefined') {
      return initialState
    }

    switch (action.type) {
      case 'SET_ACTIVE_CATEGORY':
        // state.activeCategory = action.payload.activeCategory;
        // return state;

        return Object.assign({}, state, {
          activeCategory: action.payload.activeCategory
        }); break;

      default: return state
    }
}
