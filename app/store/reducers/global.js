const initialState = {
    isAuthenticated: false,
    categoriesList: [
        'All Categories',
        'Featured',
        'Latest'
    ]
}

export default function global(state=initialState, action) {
    if (typeof state === 'undefined') {
      return initialState
    }

    switch (action.type) {

      case 'UPDATE_CATEGORIES_LIST':
          return Object.assign({}, state, {
            categoriesList: action.payload.categoriesList
          });
      break;

      case 'SET_AUTHENTICATED':
        return Object.assign({}, state, {
          isAuthenticated: true
        }); break;

      case 'SET_LOGOUT':
          return Object.assign({}, state, {
            isAuthenticated: false
          }); break;

      default: return state
    }
}
