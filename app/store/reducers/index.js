import { combineReducers } from 'redux';


import globalData from './global.js';
import homeScene from './home-route.js';
import authScene from './auth-route.js';
import categories from './categories.js';
import userProfile from './user.js';

// import rootReducer from './root-reducer.js';

export default combineReducers({
  globalData,
  homeScene,
  authScene,
  categories,
  userProfile
})
