import { AsyncStorage } from 'react-native';

export const loadState = () => {
    return new Promise((res, rej) => {
        AsyncStorage.getItem('store', (err, serializedState) => {
            console.log(serializedState);

            if (err) {
                rej(err);
            } else {
                if (serializedState === null) {
                    rej();
                } else {
                    res(JSON.parse(serializedState))
                }
            }
        })
    });
}


export const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        AsyncStorage.setItem('store', serializedState);

    } catch (err) {
        // Ignore errors of writing
        // return undefined;
    }
}
