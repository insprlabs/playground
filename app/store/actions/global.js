export function setLogout(data) {
  return {
      type: 'SET_LOGOUT',
    }
}

export function setAuthenticated(data) {
  return {
      type: 'SET_AUTHENTICATED',
    }
}
