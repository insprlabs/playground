export function displayProfile() {
  return {
      type: 'DISPLAY_PROFILE_SCREEN'
    }
}

export function displayHome() {
  return {
      type: 'DISPLAY_HOME_SCREEN'
    }
}

export function displayJobDetails(jobDetails) {
  return {
      type: 'DISPLAY_DETAILS',
      payload: {jobDetails: jobDetails}
    }
}

export function displaySearch() {
  return {
      type: 'DISPLAY_SEARCH_SCREEN'
    }
}

export function displayNewPost() {
  return {
      type: 'DISPLAY_NEW_POST'
    }
}

export function hideNewPost() {
  return {
      type: 'HIDE_NEW_POST'
    }
}

export function displayFilter() {
  return {
      type: 'DISPLAY_FILTER'
    }
}

export function hideFilter() {
  return {
      type: 'HIDE_FILTER'
    }
}

export function displayCategories() {
  return {
      type: 'DISPLAY_CATEGORIES'
    }
}

export function hideCategories() {
  return {
      type: 'HIDE_CATEGORIES'
    }
}

export function setFirstName(firstName) {
  return {
      type: 'SET_FIRST_NAME',
      payload: {firstName: firstName}
    }
}

export function displayRegistration() {
  return {
      type: 'REGISTRATE_NEW_USER'
    }
}
