export function setActiveCategory(activeCategory) {
  return {
      type: 'SET_ACTIVE_CATEGORY',
      payload: {activeCategory: activeCategory}
    }
}
