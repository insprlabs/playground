export function updateUserProfile(data) {
  return {
      type: 'UPDATE_PROFILE',
      payload: {userProfile: data}
    }
}
