import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import Root from './app';

class Playground extends Component {
    render() {
        return <Root/>
    }
}

AppRegistry.registerComponent('Playground', () => Playground);
